#!/bin/bash

if [ "${#}" != 1 ]
then
  echo "usage:"
  echo "run.sh <image-name>"
  exit 1
fi

_image="${1}"

if which podman
then
  DOCKER=podman
elif which docker
then
  DOCKER=docker
fi

if [ -f "Dockerfile.${1}" ]
then
  $DOCKER run -it "2ndfri:${_image}" bash
else
  echo "No Dockerfile found"
  exit 1
fi

